# Use the official image as a parent image.
# https://hub.docker.com/_/node
FROM node:12-slim

WORKDIR /app

# Copy package.json from host to current location.
COPY package.json .
# install dependencies in image filesystem
RUN npm install

# Copy rest of app source code and configs from host to image filesystem.
COPY . .
RUN npm run build

# Run the web service on container startup.
CMD [ "node", "build/index.js" ]
